/*
 * Application for uploading isolator data to server.
 * Copyright 2021-2026 UBT FD CVUT CZ
 */
package org.egothor.isolator;

import io.grpc.netty.NegotiationType;
import io.grpc.Grpc;

import io.netty.handler.ssl.SslContextBuilder;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.egothor.isolator.AppendRequest.Builder;
import org.apache.commons.cli.HelpFormatter; 
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import io.grpc.TlsChannelCredentials;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;
import io.netty.handler.ssl.SslContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class GRPCClient {

    static final String DEFAULT_HOSTNAME = "127.0.0.1";
    static final int DEFAULT_PORT = 55000;
    static final Logger logger = LoggerFactory.getLogger("GPRCCLient");
    
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static void main(String[] args) {        
        String hostname = DEFAULT_HOSTNAME;
        int port = DEFAULT_PORT;
        String messageId = null;
        String message = "";
        String clientCert = null;
        String clientKey = null;
        String caCert = null;
        String fileImage = null;
               
        Options options = new Options();
        
        Option optionHostname = new Option("h", "hostname", true, "Server hostname");
        optionHostname.setRequired(false);
        options.addOption(optionHostname);
        
        Option optionPort = new Option("p", "port", true, "Server port");
        optionPort.setRequired(false);
        options.addOption(optionPort);
        
        Option optionID = new Option("i", "id", true, "Message ID");
        optionID.setRequired(false);
        options.addOption(optionID);
        
        Option optionMessage = new Option("m", "message", true, "JSON message");
        optionMessage.setRequired(false);
        options.addOption(optionMessage);
        
        Option optionIdendity = new Option("c", "client-cert", true, "Certificate - client-cert");
        optionIdendity.setRequired(true);
        options.addOption(optionIdendity);
        
        Option optionTruststore = new Option("k", "client-key", true, "Certificate - client-key");
        optionTruststore.setRequired(true);
        options.addOption(optionTruststore);
        
        Option optionCACert = new Option("s", "server-cert", true, "Server certificate - CA");
        optionCACert.setRequired(true);
        options.addOption(optionCACert);
        
        Option optionFile = new Option("f", "file", true, "Image file name (jpeg format)");
        optionFile.setRequired(false);
        options.addOption(optionFile);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine commandLine = null;
               
        try {
            commandLine = parser.parse(options, args);
            if (commandLine.hasOption("h")) {
                hostname = commandLine.getOptionValue("h");
            } 
            if (commandLine.hasOption("p")) {
                port = Integer.parseInt(commandLine.getOptionValue("p"));
            }
            if (commandLine.hasOption("i")) {
                messageId = commandLine.getOptionValue("i");
            }
            if (commandLine.hasOption("m")) {
            	message = commandLine.getOptionValue("m");
            } else {            	
            	@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);
            	while(sc.hasNextLine()){
                    String sor=sc.nextLine();
                    message += sor;
                }
            }
            clientCert = commandLine.getOptionValue("c");
            clientKey = commandLine.getOptionValue("k");
            caCert = commandLine.getOptionValue("s");
            if (commandLine.hasOption("f")) {
                fileImage = commandLine.getOptionValue("f");
            }
        } catch (ParseException exception) {
            logger.error("Argument parse error: ");
            logger.error(exception.getMessage());
            formatter.printHelp("utility-name", options);
            
            System.exit(1);
        }         
        
        try {
        	File fImage = null;        
            File fileClientCert = new File(clientCert);
            if (!fileClientCert.exists()) {
                throw new Exception("Input file (client certificate) does not exist!");
            }
            File fileClientKey = new File(clientKey);
            if (!fileClientKey.exists()) {
                throw new Exception("Input file (client key) does not exist!");
            }
            File fileCACert = new File(caCert);
            if (!fileCACert.exists()) {
                throw new Exception("Input file (CA) does not exist!");
            }
            
            if (fileImage != null) {
            	fImage = new File(fileImage);
            	if (!fImage.exists()) {
                    throw new Exception("Image file does not exist!");
                }
            }

            ManagedChannel channel = null;

            try {
                TlsChannelCredentials.Builder tlsBuilder = TlsChannelCredentials.newBuilder();
                tlsBuilder.keyManager(fileClientCert, fileClientKey);
                tlsBuilder.trustManager(fileCACert);

                channel = Grpc.newChannelBuilderForAddress(hostname, port, tlsBuilder.build())
                        .overrideAuthority("server.isolator").build();

                org.egothor.isolator.DatabaseGrpc.DatabaseBlockingStub stub = org.egothor.isolator.DatabaseGrpc.newBlockingStub(channel);
                Builder builder = AppendRequest.newBuilder().setJsonData(message);
                if (fImage != null) {
                	
                	AppendRequest.Attachment attachment = AppendRequest.Attachment.newBuilder().setContentType("image/jpeg").setData(ByteString.readFrom(new FileInputStream(fImage))).build();
                	builder.addBlob(attachment);
                }                 
                AppendRequest request = builder.build();
                
                /*AppendRequest request = AppendRequest.newBuilder().setJsonData(message)
                		.addBlob(AppendRequest.Attachment.newBuilder().setContentType("text/html").setData(ByteString.copyFromUtf8("<html>test</html>")).build())
                		.addBlob(AppendRequest.Attachment.newBuilder().setContentType("text/plain").setData(ByteString.copyFromUtf8("testtext")).build())
                		.build(); */
                AppendReply reply = stub.append(request);

                System.out.println("Status: "+reply.getStatus());
            } finally {
                if (channel != null)
                channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
            } 
            System.exit(0);
            
          //  logger.info(String.format("Response: %s", response.getStatus()));
        } catch (StatusRuntimeException exception) {
            logger.error("ApendRequest error: ");            
            logger.error(exception.getMessage());
            System.exit(1);
        } catch (Exception exception) {
        	logger.error("Error: ");
            logger.error(exception.getMessage());
            
            System.exit(1);
        }

    }
}

