package org.egothor.isolator;

import io.grpc.Grpc;
import io.grpc.ManagedChannel;
import io.grpc.TlsChannelCredentials;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Client {

    public Client() {

    }

    public void run() throws IOException, InterruptedException {
        ManagedChannel channel = null;

        try {
            TlsChannelCredentials.Builder tlsBuilder = TlsChannelCredentials.newBuilder();
            tlsBuilder.keyManager(
                    getClass().getClassLoader().getResourceAsStream("client-cert.pem"),
                    getClass().getClassLoader().getResourceAsStream("client-key.pem"));
            tlsBuilder.trustManager(
                    getClass().getClassLoader().getResourceAsStream("ca-cert.pem"));

            channel = Grpc.newChannelBuilderForAddress("127.0.0.1", 55000, tlsBuilder.build())
                    .overrideAuthority("server.isolator").build();

            org.egothor.isolator.DatabaseGrpc.DatabaseBlockingStub stub = org.egothor.isolator.DatabaseGrpc.newBlockingStub(channel);

            ByteArrayOutputStream baos = new ByteArrayOutputStream(8*1024);
            getClass().getClassLoader().getResourceAsStream("test.json").transferTo(baos);

            String testStr = baos.toString("UTF-8");

            // "{test:5,build:4,pole:[{a:8,b:4},{a:4,b:2}]}"

            AppendRequest request = AppendRequest.newBuilder().setJsonData(testStr).build();
            AppendReply reply = stub.append(request);

            System.out.println("Status: "+reply.getStatus());
        } finally {
            if (channel != null)
            channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
        }
    }

    
}
