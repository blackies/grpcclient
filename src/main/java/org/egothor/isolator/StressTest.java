/*
 * Application for uploading isolator data to server.
 * Copyright 2021-2026 UBT FD CVUT CZ
 */
package org.egothor.isolator;

import io.grpc.ChannelCredentials;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter; 
import io.grpc.StatusRuntimeException;
import io.grpc.TlsChannelCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.concurrent.Phaser;

public class StressTest {

    static final String DEFAULT_HOSTNAME = "127.0.0.1";
    static final int DEFAULT_PORT = 55000;
    static final int MAX_THREADS_NUMBER = 256;
    static final Logger logger = LoggerFactory.getLogger("GPRCCLient");

    public static void main(String[] args) {        
        String hostname = DEFAULT_HOSTNAME;
        int port = DEFAULT_PORT;
        String clientCert = null;
        String clientKey = null;
        String caCert = null;
               
        Options options = new Options();
        
        Option optionHostname = new Option("h", "hostname", true, "Server hostname");
        optionHostname.setRequired(false);
        options.addOption(optionHostname);
        
        Option optionPort = new Option("p", "port", true, "Server port");
        optionPort.setRequired(false);
        options.addOption(optionPort);
        
        Option optionNumber = new Option("n", "number", true, "Number of threads");
        optionNumber.setRequired(true);
        options.addOption(optionNumber);
        
        Option optionMessages = new Option("m", "messages", true, "Messages per thread");
        optionMessages.setRequired(true);
        options.addOption(optionMessages);
        
        Option optionIdendity = new Option("c", "clientcert", true, "Certificate - client-cert");
        optionIdendity.setRequired(true);
        options.addOption(optionIdendity);
        
        Option optionTruststore = new Option("k", "clientkey", true, "Certificate - client-key");
        optionTruststore.setRequired(true);
        options.addOption(optionTruststore);
        
        Option optionCACert = new Option("s", "cacert", true, "Certificate - CA");
        optionCACert.setRequired(true);
        options.addOption(optionCACert);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine commandLine = null;
               
        try {
            commandLine = parser.parse(options, args);
            if (commandLine.hasOption("h")) {
                hostname = commandLine.getOptionValue("h");
            } 
            if (commandLine.hasOption("p")) {
                port = Integer.parseInt(commandLine.getOptionValue("p"));
            }
           
            final int number = Integer.parseInt(commandLine.getOptionValue("n"));
            int messages = Integer.parseInt(commandLine.getOptionValue("m"));
            clientCert = commandLine.getOptionValue("c");
            clientKey = commandLine.getOptionValue("k");
            caCert = commandLine.getOptionValue("s");

            File fileClientCert = new File(clientCert);
            if (!fileClientCert.exists()) {
                throw new Exception("Input file (client certificate) does not exist!");
            }
            File fileClientKey = new File(clientKey);
            if (!fileClientKey.exists()) {
                throw new Exception("Input file (client key) does not exist!");
            }
            File fileCACert = new File(caCert);
            if (!fileCACert.exists()) {
                throw new Exception("Input file (CA) does not exist!");
            }

            TlsChannelCredentials.Builder tlsBuilder = TlsChannelCredentials.newBuilder();
            tlsBuilder.keyManager(fileClientCert, fileClientKey);
            tlsBuilder.trustManager(fileCACert);
            ChannelCredentials credentials = tlsBuilder.build();
            
            final String testStr =  "{test:2,messagge:3,de:222,pole:[{a:8,b:4},{a:4,b:0}]}";
            
            Phaser phaser = new Phaser();
            phaser.register();
            
            PhaserWorker workers[] = new PhaserWorker[number];
            for (int i = 0; i < number; i++) {
            	workers[i] = new PhaserWorker(phaser, credentials, "Worker " + i, messages);
            	workers[i].start();
            }
            phaser.arriveAndAwaitAdvance();
            
            //System.exit(0);
            
          //  logger.info(String.format("Response: %s", response.getStatus()));
        } catch (ParseException exception) {
                logger.error("Argument parse error: ");
                logger.error(exception.getMessage());
                formatter.printHelp("utility-name", options);
                
                System.exit(1);         
        } catch (StatusRuntimeException exception) {
            logger.error("ApendRequest error: ");
            logger.error(exception.getMessage());
            
            System.exit(1);
        } catch (Exception exception) {
        	logger.error("Error: ");
            logger.error(exception.getMessage());
            
            System.exit(1);
        }

    }
}

