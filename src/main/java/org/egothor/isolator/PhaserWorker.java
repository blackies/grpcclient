package org.egothor.isolator;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

import io.grpc.ChannelCredentials;
import io.grpc.Grpc;
import io.grpc.ManagedChannel;
import org.slf4j.*;
import java.time.Instant;

public class PhaserWorker extends Thread {
    private Phaser phaser;
    private int messages;
    private ChannelCredentials credentials;
    
    static final Logger logger = LoggerFactory.getLogger("PhaserWorker");

    public PhaserWorker(Phaser phaser, ChannelCredentials credentials, String name, int messages) {
        this.phaser = phaser;
        this.credentials = credentials;
        this.messages = messages;
        phaser.register();
        setName(name);
    }

    @Override public void run() {
    	String testStr = "{test:123}";
    	phaser.arriveAndAwaitAdvance();
		for (int j = 0; j < messages; j++) {
			ManagedChannel channel = null;
      		  try {	   
      			  channel = Grpc.newChannelBuilderForAddress("127.0.0.1", 55000, credentials)
                        .overrideAuthority("server.isolator").build();
                  org.egothor.isolator.DatabaseGrpc.DatabaseBlockingStub stub = org.egothor.isolator.DatabaseGrpc.newBlockingStub(channel);
                  AppendRequest request = AppendRequest.newBuilder().setJsonData(testStr).build();                  
                  logger.info(String.format("Index: %d, GPRC sending at: %s", j, Instant.now()));
          		  AppendReply reply = stub.append(request);
                  logger.info(String.format("Index: %d - status: %s at: %s", j, reply.getStatus(), Instant.now()));
      		  } catch (Exception e) {
              	  logger.error("GRPC process error: " + e.getMessage());
                } finally {
              	  if (channel != null) {
              		  try {
              			  channel.shutdown().awaitTermination(2, TimeUnit.SECONDS);
              		  } catch (Exception ex) {
              			  logger.error("Error channel awaitTermination: " + ex.getMessage());
              		  }
              	  }
                }
      	  }         
    }
}
