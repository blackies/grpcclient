package org.egothor.isolator;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.41.0)",
    comments = "Source: isolator.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class DatabaseGrpc {

  private DatabaseGrpc() {}

  public static final String SERVICE_NAME = "org.egothor.isolator.Database";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<org.egothor.isolator.AppendRequest,
      org.egothor.isolator.AppendReply> getAppendMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "append",
      requestType = org.egothor.isolator.AppendRequest.class,
      responseType = org.egothor.isolator.AppendReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<org.egothor.isolator.AppendRequest,
      org.egothor.isolator.AppendReply> getAppendMethod() {
    io.grpc.MethodDescriptor<org.egothor.isolator.AppendRequest, org.egothor.isolator.AppendReply> getAppendMethod;
    if ((getAppendMethod = DatabaseGrpc.getAppendMethod) == null) {
      synchronized (DatabaseGrpc.class) {
        if ((getAppendMethod = DatabaseGrpc.getAppendMethod) == null) {
          DatabaseGrpc.getAppendMethod = getAppendMethod =
              io.grpc.MethodDescriptor.<org.egothor.isolator.AppendRequest, org.egothor.isolator.AppendReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "append"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.egothor.isolator.AppendRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  org.egothor.isolator.AppendReply.getDefaultInstance()))
              .setSchemaDescriptor(new DatabaseMethodDescriptorSupplier("append"))
              .build();
        }
      }
    }
    return getAppendMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DatabaseStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DatabaseStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DatabaseStub>() {
        @java.lang.Override
        public DatabaseStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DatabaseStub(channel, callOptions);
        }
      };
    return DatabaseStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DatabaseBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DatabaseBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DatabaseBlockingStub>() {
        @java.lang.Override
        public DatabaseBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DatabaseBlockingStub(channel, callOptions);
        }
      };
    return DatabaseBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DatabaseFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DatabaseFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DatabaseFutureStub>() {
        @java.lang.Override
        public DatabaseFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DatabaseFutureStub(channel, callOptions);
        }
      };
    return DatabaseFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class DatabaseImplBase implements io.grpc.BindableService {

    /**
     */
    public void append(org.egothor.isolator.AppendRequest request,
        io.grpc.stub.StreamObserver<org.egothor.isolator.AppendReply> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAppendMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getAppendMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                org.egothor.isolator.AppendRequest,
                org.egothor.isolator.AppendReply>(
                  this, METHODID_APPEND)))
          .build();
    }
  }

  /**
   */
  public static final class DatabaseStub extends io.grpc.stub.AbstractAsyncStub<DatabaseStub> {
    private DatabaseStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DatabaseStub(channel, callOptions);
    }

    /**
     */
    public void append(org.egothor.isolator.AppendRequest request,
        io.grpc.stub.StreamObserver<org.egothor.isolator.AppendReply> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAppendMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DatabaseBlockingStub extends io.grpc.stub.AbstractBlockingStub<DatabaseBlockingStub> {
    private DatabaseBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DatabaseBlockingStub(channel, callOptions);
    }

    /**
     */
    public org.egothor.isolator.AppendReply append(org.egothor.isolator.AppendRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAppendMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DatabaseFutureStub extends io.grpc.stub.AbstractFutureStub<DatabaseFutureStub> {
    private DatabaseFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DatabaseFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DatabaseFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<org.egothor.isolator.AppendReply> append(
        org.egothor.isolator.AppendRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAppendMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_APPEND = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DatabaseImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DatabaseImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_APPEND:
          serviceImpl.append((org.egothor.isolator.AppendRequest) request,
              (io.grpc.stub.StreamObserver<org.egothor.isolator.AppendReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DatabaseBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DatabaseBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return org.egothor.isolator.DatabaseProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Database");
    }
  }

  private static final class DatabaseFileDescriptorSupplier
      extends DatabaseBaseDescriptorSupplier {
    DatabaseFileDescriptorSupplier() {}
  }

  private static final class DatabaseMethodDescriptorSupplier
      extends DatabaseBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DatabaseMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DatabaseGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DatabaseFileDescriptorSupplier())
              .addMethod(getAppendMethod())
              .build();
        }
      }
    }
    return result;
  }
}
