Pro testovani je potreba pustit server isolator2-1.0-SNAPCHOT-jar-with-dependencies.jar (java -jar isolator2-1.0-SNAPCHOT-jar-with-dependencies.jar)
Klient je v GRPCClient.jar, potrebne soubory jsou: ca-cert.pem, client-cert.pem a client-key.pem. Klient se pousti nasledovne:
java -jar GRPCClient.jar -h isolator.server -p 55000 -k client-key.pem -c client-cert.pem -s ca-cert.pem -m {test:1233} -i ID_MERENI

ID mereni neni potreba zadavat, kdyz se posila cela zprava najednou.
Pokud nebude vyplneny parametr -m v prikazove radce, tak se az do EOF zadava json jako vstup.